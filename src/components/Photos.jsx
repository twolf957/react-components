import PropTypes from "prop-types"
import React from 'react'

const Photos = ({ files }) => {
  return (
    <div className="photoHolder">
      {files.map((photo) => (
        <div key={photo.id} className="photo-item">
          <p>{photo.title}</p>
          <img src={photo.url} alt={`${photo.id}`} />
        </div>
      ))}
    </div>
  );
};

Photos.propTypes = {
  files: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.number.isRequired,
      url: PropTypes.string.isRequired,
    })
  ).isRequired,
};


export default Photos