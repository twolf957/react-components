import React from 'react'

const Header = ({title}) => {
  return (
    <h1>{title ? title : "Header title"}</h1>
  )
}

export default Header