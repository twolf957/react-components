import React from 'react'

const Main = ({children}) => {
  return (
    <div>
        <h2>Pictures provided below</h2>
        {children}
    </div>
  )
}

export default Main