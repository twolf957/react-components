import React from 'react'

const Footer = ({title}) => {
  return (
    <h2>{title ? title : "Footer title"}</h2>
  )
}

export default Footer